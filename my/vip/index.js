// pages/vip/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl:'',
    name:'',
    list:[]
  },
  openvip(e){
    wx.navigateTo({
      url: '/my/openVip/index?type=vip'+'&id='+e.currentTarget.dataset.id + '&productId='+ this.data.list[e.currentTarget.dataset.index].productId,
    })
  },
  //替换时间戳
  transTime(unixtime) {
    if(unixtime.length === 10){
      var dateTime = new Date(parseInt(unixtime) * 1000)
    }else{
      var dateTime = new Date(parseInt(unixtime))
    }
      var year = dateTime.getFullYear();
      var month = dateTime.getMonth() + 1;
      if(month < 10){
        month  = '0'+month
      }else{
        month = month
      }
      var day = dateTime.getDate();
      if(day < 10){
        day  = '0'+day
      }else{
        day = day
      }
      var hour = dateTime.getHours();
      if(hour < 10){
        hour  = '0'+hour
      }else{
        hour = hour
      }
      var minute = dateTime.getMinutes();
      if(minute < 10){
        minute  = '0'+minute
      }else{
        minute = minute
      }
      var second = dateTime.getSeconds();
      if(second < 10){
        second  = '0'+second
      }else{
        second = second
      }
      var now = new Date();
      var now_new = Date.parse(now.toDateString());
      var milliseconds = now_new - dateTime;
      var timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
      return timeSpanStr;
    },
  //获取有效的套餐列表
  getList(){
    var data = {
      mac:wx.getStorageSync('uuid'),
    }
    app.getRequest('/boapi/v1/product/effectiveEdu', data, (res) => {
      console.log('effectiveEdu',res)
      res.products.forEach((item,index)=>{
        res.products[index].effectiveTime = this.transTime(res.products[index].effectiveTime)
        res.products[index].expiredTime = this.transTime(res.products[index].expiredTime)
      })
      this.setData({
        list:res.products
      })
    }, (err) => { })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.setData({
      imgurl:options.url,
      name:options.name
    })
    this.getList()

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})