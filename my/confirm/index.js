// pages/confirm/index.js
var zhmd5 = require('../../utils/md5.js')
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    token:'',
    deviceType:'',
    deviceModel:'',
    sign: zhmd5.md5("/v1/auth/login?X-apiId=123456&X-deviceType='+'iphone&X-languageCode=zh_CN&X-mac=FC:D5:D9:02:FE:72&X-requestToken=sdmcsaas&X-tenantCode=sdmcsaas&X-timstamp=1585106793321&X-version=4.3.1&{'userAccessToken':'A68E84C9-83A0-469B-8D1F-F49A100C9900','mac':'0','username':'','password':'',loginType:'wechat-miniprogram'}&X-apiKey=e1e499e5746f90191ec5261947055e0c")
  },
  //调起微信支付
  pay(){
    wx.requestPayment({
      timeStamp: '1597133586',
      nonceStr: 'UF5t1vU7cdlXLB1Sb7Cluf7wPYTPjKYa',
      package: 'prepay_id=wx11161305068794599dbb4b19f9bb290000',
      signType: 'HMACSHA256',
      paySign: '4BD47453D5BB7F0B2FA5A37CE08048CF4F00D0A75C6791C2704B3F9394757FFF',
      success (res) { 
        console.log('支付成功',res)
        wx.showToast({
          title: '支付成功',
        })
      },
      fail (res) { 
        console.log('支付失败',res)
      }
    })
  },
  //返回上一页
  back(){
    wx.navigateBack({
      delta: 1,
    })
  },
  //获取手机号
  getPhoneNumber (e) {
    let that = this;
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
    let info = wx.getStorageSync('userInfo')
    if (e.detail.errMsg === 'getPhoneNumber:ok') {
      // 这里处理允许逻辑
      var data = {
        encryptedData: e.detail.encryptedData,
        iv: e.detail.iv,
        nickName:info.nickName,
        gender:info.gender,
        city:info.city,
        province:info.province,
        country:info.country,
        avatarUrl:info.avatarUrl
      }
      app.getRequest('/boapi/v1/member/userInfo/update', data, (res) => {
        console.log('接口返回update',res)
        if (!res.error) {
            console.log('res',res)
            wx.setStorageSync('isLogin', true) 
            wx.setStorageSync('phoneNumber', res.phoneNumber)
            wx.reLaunch({
              url: '/pages/index/index',
            })
        }else{
          wx.showToast({
            title: '网络有点问题，请稍后重试！',
          })

        }
      }, (err) => { })
     } else {
      // 这里是拒绝或者没绑定手机号等其他逻辑。我们一般这里不写任何东西。只处理上边部分。
      console.log('拒绝授权')
     }
   
          
    
  },
   getInfo(){
    let that = this;
    wx.login({
      success (res) {
        if (res.code) {
          console.log(res)
          console.log('code',res.code);
          var data = {
            userAccessToken: res.code,
            username: res.code,
            loginType:'wechat-miniprogram'
          }
          app.getRequest('/boapi/v1/auth/login', data, (res) => {
            console.log('接口返回getinfo',res)
            if (res.resultCode === 0) {
                console.log('res',res)
                app.globalData.token = res.token;
                wx.setStorageSync('token', res.token)
                console.log('tokenurl',app.globalData.token)
            }else{
              wx.showToast({
                title: '网络有点问题，请稍后重试！',
              })

            }
          }, (err) => { })
          
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
   },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('md5',this.data.token);
    let that = this;
    

  },
 

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})