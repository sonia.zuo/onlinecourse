// pages/collect/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    left:0,
    top:0,
    listData:[],
    feed_style:{
      x:"",
      y:"",
      },
      //这个参数是定位使用的x ， y值
      // 仅在js里面传递的参数：
      screen:{
              width:"",
              height:""
      
      }, // 用于保存屏幕页面信息,上次的x值,上次的y值
      preX:'',
      preY:'',

  },
  onClose(event) {
    let that = this;
    console.log('event',event)
    const { position, instance } = event.detail;
    switch (position) {
      case 'left':
      case 'cell':
        instance.close();
        break;
      case 'right':
        wx.showModal({
          title: '提示',
          content: '确认删除吗？',
          success (res) {
            if (res.confirm) {
              console.log('用户点击确定')
              instance.close();
              that.delList(event.currentTarget.id)
            } else if (res.cancel) {
              console.log('用户点击取消')
              instance.close();
            }
          }
        })
        break;
    }
  },
  swipeOpen(e){
    console.log('swipeopen')
    let index = e.currentTarget.dataset.index
    this.selectComponent('#swipe-'+index).open('right');
},
  opencourse(){
    wx.switchTab({
      url: '/pages/course/index',
    })
  },
  viewTouchMove:function(e){
    console.log(e.currentTarget.id)
    var left = "listData[" + e.currentTarget.id+ "].left";//先用一个变量，把(info[0].gMoney)用字符串拼接起来
    var top = "listData[" + e.currentTarget.id+ "].top";
    this.setData({
        [left]:e.touches[0].clientX-20,
        [top]:e.touches[0].clientY-20
    })
    console.log('list',this.data.listData)

},
touchMoveChange(e){
  var tmpx = parseInt(e.touches[0].clientX);
  var tmpy = parseInt(e.touches[0].clientY);
  if (tmpx <= 0 || tmpy <= 0 || tmpx >= this.data.screen.width || tmpy >= this.data.screen.height ){
  }else{
  if (tmpx != this.data.preX || tmpy != this.data.preY ){
  console.log(e.touches[0].clientX, "-X-", e.touches[0].pageX)
  console.log(e.touches[0].clientY, "-Y-", e.touches[0].pageY)
  this.data.preX = tmpx
  this.data.preY = tmpy 
  this.setData({
  feed_style: {
  x: tmpx - 50 + "px",
  y: tmpy - 50 + "px",
  }
  })
  console.log(e.currentTarget.id)
    var left = "listData[" + e.currentTarget.id+ "].left";//先用一个变量，把(info[0].gMoney)用字符串拼接起来
    var top = "listData[" + e.currentTarget.id+ "].top";
    var position = "listData[" + e.currentTarget.id+ "].position";
    this.setData({
        [left]:this.data.feed_style.x,
        [top]:this.data.feed_style.y,
        [position]:'absolute'
    })
    console.log('list',this.data.listData)
  }
  }
  },
  	//获取收藏列表
	getList(){
		var data = {
			mac:wx.getStorageSync('uuid'),
			type:'vod',
			pageSize:15,
			page:0

		}
		app.getRequest('/boapi/v1/favorites/list', data,  (res) => {
			if (!res.error) {
      console.log('collgetListctLesson',res)
      this.setData({
        listData:res.contents
      })
			
			}
		}, (err) => { })

  },
  toLesson(e){
    console.log('id',e.currentTarget.dataset.id)
    console.log('index',e.currentTarget.dataset.index)
    wx.navigateTo({
      url: '/course/courseInfo/index?id='+e.currentTarget.dataset.id + '&isVip='+this.data.listData[e.currentTarget.dataset.index].isVip,
    })
  },
  //删除收藏
		delList(id){
			console.log('id',id)
			var data = {
				assetId:id,
				type:'vod',
				action: '0'
			}
			app.getRequest('/boapi/v1/favorites/record', data,  (res) => {
				if (!res.error) {
        console.log('删除收藏',res)
        wx.showToast({
          title: '删除成功！',
          icon:'none'
        })
				this.getList();
				}
			}, (err) => { })
		},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    this.getList();
    wx.getSystemInfo({

      success: function (res) {
      
      console.log(res);
      
      console.log("platform",res.platform);
      
      console.log(res.model);
      
      // 可使用窗口宽度、高度
      
      console.log('height=' + res.windowHeight);
      
      console.log('width=' + res.windowWidth);
      
      // Math.ceil()
      
      if(res.platform == "android"){
      
      res.windowHeight = res.screenHeight;
      
      }
      
      // feed_style: {
      
      // x: res.windowWidth + "px",
      
      // y: res.windowHeight + "px"
      
      // },
      
      that.setData({
      
      screen:{
      
      width: res.windowWidth ,
      
      height: res.windowHeight ,
      
      pixelRatio: res.pixelRatio,
      
      ratio: res.windowWidth * res.pixelRatio/750
      
      }
      
      })
      
      // 计算主体部分高度,单位为px
      
      // that.setData({
      
      // second_height: res.windowHeight
      
      // })
      
      }
      
      })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})