// pages/openVip/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    select:0,
    list:[],
    check:'',
    listData:[],
    cycleUnit:'',
    sum:'',
    productId:'',
    productName:'',
    contentId:'',
    type:'',
    isShow:true,
    selectPackage:0,
    productId:''
  },
  //选中学段
  selectIndex(e){
    console.log('e',e.currentTarget.dataset.index)
    if(this.data.contentId){
      return
    }else{
      this.setData({
        select:e.currentTarget.dataset.index,
        check:''
      })
      let id = this.data.list[this.data.select].categoryId.toString()
      this.getList(id)

    }
    
  },
  //选中年级
  checkIndex(e){
    if(this.data.contentId){
      return
    }else{
      this.setData({
        check:e.currentTarget.dataset.index
      })
      let id = this.data.list[this.data.select].categorys[this.data.check].categoryId.toString()
      this.getList(id)

    }
   
  },
  //选中套餐
  timeSelect(e){
    console.log('index',e.currentTarget.dataset.index)
     this.setData({
       selectPackage: e.currentTarget.dataset.index,
       sum: this.data.listData[e.currentTarget.dataset.index].price,
       productId: this.data.listData[e.currentTarget.dataset.index].productId,
       productName: this.data.listData[e.currentTarget.dataset.index].productName,
     })
  },
  //获取学段
  getVip(){
    let that = this;
    var data = {
      mediaType:'vod',
      categoryId:0,
      mac:wx.getStorageSync('uuid')
    }
    app.getRequest('/boapi/v1/content/category', data,  (res) => {
      if (!res.error) {
      console.log('list',res)
      this.setData({
        list:res.categorys
      })
      //从课程详情进入页面
      if(this.data.contentId && this.data.type === 'info'){
        this.getList(this.data.contentId)
      }else{
        //从我的模块进入套餐包
        if(this.data.type === 'mine'){
          let id = this.data.list[0].categoryId.toString()
          this.getList(id)
        }else{
          //从我的vip续费进入套餐包
          console.log('id',this.data.list)
          this.data.list.forEach((item,index)=>{
             if(this.data.contentId === item.categoryId){
               this.setData({
                 select:index
               })
             }
             this.data.list[index].categorys.forEach((item1,index1)=>{
               if(this.data.contentId === item1.categoryId){
                 this.setData({
                   select: index,
                   check:index1
                 })
               }
             })
          })
          this.getList(this.data.contentId)
        }
        
      }
     
      
      }
    }, (err) => { })

  },
  //确认订单
  confirm(){
    if(this.data.sum === 0){
      return
    }else{
      wx.navigateTo({
        url: '/order/orderConfirm/index?productName='+this.data.productName+'&price='+this.data.sum + '&contentId='+this.data.contentId+'&productId='+this.data.productId,
      })

    }
    

  },
  //可开通套餐列表
  getList(id){
    let that = this;
    if(this.data.contentId && this.data.type === 'info'){
      var data = {
        contentId:id,
        pageSize:20,
        page:0,
      }
      this.setData({
        isShow:false
      })
    }else{
      var data = {
        categoryId:id,
        pageSize:20,
        page:0
      }
      this.setData({
        isShow:true
      })
    }
    app.getRequest('/boapi/v1/product/listEdu', data,  (res) => {
      if (!res.error) {
      console.log('list',res)
      this.setData({
        listData:[],
        sum:0,
      })
      let list = [];
      list = res.products;
      if(list.length !== 0){
        list.forEach((item,index)=>{
          if(item.cycleUnit === 'Month'){
            list[index].cycleUnit = '月'
          }
          if(item.cycleUnit === 'Year'){
            list[index].cycleUnit = '年'
          }
        })
        this.setData({
          listData:list,
          sum:list[0].price,
          productId:list[0].productId,
          productName: list[0].productName
        })
       

      }
      console.log('listdata',this.data.listData)
     
      
      }
    }, (err) => { })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('options',options)
    // this.setData({
    //   contentId:'',
    //   type:'',
    //   listData:[]
    // })
    this.setData({
      contentId:options.id,
      type:options.type
    })
    this.getVip()
   
    
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})