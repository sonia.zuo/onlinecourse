// pages/course/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgDown:'../../../assets/images/course_down.png',
    imgUp:'../../../assets/images/course_up.png',
    page:0,
    listData:[],
    name:'',
    page:0,
    pageAdd:'',
    id:''
  },
   //课程切换
   lessonOrder(){
    console.log('课程排序切换')
   },
   //综合排序切换
   totalOrder(){
     console.log('综合排序切换')
   },
   //获取课程列表
    //获取课程数据
  getList(id){
    let that = this;
      var data = {
        mac:wx.getStorageSync('uuid'),
        mediaType:'vod',
        categoryId:id,
        pageSize:15,
        page:this.data.page,
        sort:'time'
      }
    console.log('data',data)
      app.getRequest('/boapi/v1/content/list', data,  (res) => {
        if (!res.error) {
        console.log('获取课程列表',res)
        let content = res.contents
        if(content.length === 0){
          wx.showToast({
            title: '已加载全部课程',
            icon:'none'
          })
        }
        if(this.data.pageAdd){
          if(content){
            that.setData({
              listData:this.data.listData.concat(content),
              pageCount:res.pageCount,
            })
          }
          if (content.length>0){
            // this.data.page++
          }else{
            this.data.pageAdd=''
          }
          
        }else{
          that.setData({
            listData:res.contents,
            pageCount:res.pageCount,
          })
        }
        console.log('list',this.data.listData)
       
        }
      }, (err) => { })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList(options.id)
    this.setData({
      name:options.name,
      id: options.id
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
   //解决tabbar渲染延迟问题
   onShow(){
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        active: 1
      })
    }
  },
//跳转课程详情
toLesson(){
  wx.navigateTo({
    url: '/pages/courseInfo/index',
  })
},
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
   //搜索输入框
   toSearch(e) {
    wx.navigateTo({
      url: '/course/searchCourse/index',
    })
  },
  //进入详情
  toLesson(e){
    wx.navigateTo({
      url: '/course/courseInfo/index?id='+e.currentTarget.dataset.id + '&isVip='+this.data.listData[e.currentTarget.dataset.index].isVip,
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
      console.log('上拉加载')
      this.setData({
        page:this.data.page +1,
        pageAdd:true
      })
      console.log('上拉加载',this.data.page)
     this.getList(this.data.id)
      
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})