// pages/course/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categoryId:'',
    isLogin:'',
    list:[
      {name:'123'}
    ],
    menuList:[],
    chooseText:'全部课程',
    sortText:'综合排序',
    sortIndex:'0',
    dataIndex:'',
    show:false,
    active:0,
    selected:0,
    grade:0,
    imgDown:'../../assets/images/course_down.png',
    imgDown1:'../../assets/images/course_down.png',
    imgUp:'../../assets/images/course_up.png',
    listData:[],
    pageCount:'',
    page:0,
    pageSize:15,
    pageAdd:'',
  },
  //获取课程筛选数据结构
  getSearch(){
    let that = this;
      var data = {
        mac:wx.getStorageSync('uuid'),
        mediaType:app.globalData.menuList[1].type,
        categoryId:0
      }
      app.getRequest('/boapi/v1/content/category', data,  (res) => {
        if (!res.error) {
        console.log('获取分类列表',res)
        let list = res.categorys;
        var total = {
          categoryId:'all',
          categoryName:'全部课程',
          sort:'',
          mediaType:'vod',
          categorys:[
            {
              categoryId:'all',
              categoryName:'全部课程',
              sort:'',
              mediaType:'vod'
            }
          ]
        }
        list.unshift(total)
        that.setData({
          menuList:list
        })
        console.log('menuList',that.data.menuList)
        if(!this.data.categoryId){
          this.getList('all','vod','',this.data.page)
        }else{
          this.getList(this.data.categoryId,'vod','',this.data.page)
        }
       
        }
      }, (err) => { })

  },
  //获取课程数据
  getList(categoryId,type,sort,page){
    let that = this;
    if(sort){
      var data = {
        mac:wx.getStorageSync('uuid'),
        mediaType:type,
        categoryId:categoryId,
        sort:sort,
        pageSize:15,
        page:page
      }

    }else{
      var data = {
        mac:wx.getStorageSync('uuid'),
        mediaType:type,
        categoryId:categoryId,
        pageSize:15,
        page:page
      }
    }
    console.log('data',data)
      app.getRequest('/boapi/v1/content/list', data,  (res) => {
        if (!res.error) {
        console.log('获取课程列表',res)
        let content = res.contents
        if(this.data.pageAdd){
          if(content){
            that.setData({
              listData:this.data.listData.concat(content),
              pageCount:res.pageCount
            })
          }
          if (content.length>0){
            this.data.page++
          }else{
            this.data.pageAdd=''
          }
          
        }else{
          that.setData({
            listData:res.contents,
            pageCount:res.pageCount
          })
        }
       
        }
      }, (err) => { })
  },
  //选择年级
  choose(e){
    console.log('index',e);
    console.log('index',e.currentTarget.dataset.index);
    this.setData({
      active:e.currentTarget.dataset.index,
      selected:e.currentTarget.dataset.index,
      grade:0
    })
    console.log('selected',this.data.selected)
  },
  //chooseGrade
  chooseGrade(e){
    this.setData({
      page:0,
      pageAdd:false
    })
    this.setData({
      grade:e.currentTarget.dataset.index,
      show:false,
      chooseText: this.data.menuList[this.data.selected].categorys[e.currentTarget.dataset.index].categoryName ,
      imgDown:'../../assets/images/course_down.png'
    })
    this.getList(e.currentTarget.dataset.id,e.currentTarget.dataset.type,'',this.data.page)
  },
  isShow(){
    this.setData({
      imgDown:'../../assets/images/course_down.png',
      show:false
    })
  },
   //课程切换
   lessonOrder(){
    console.log('课程排序切换')
    this.setData({
      page:0,
      pageAdd:false
    })
    if(this.data.imgDown === '../../assets/images/course_down.png'){
      this.setData({
        imgDown:this.data.imgUp,
        show:true,
        dataIndex:0,
        imgDown1:'../../assets/images/course_down.png',
      })
    }else{
      this.setData({
        imgDown:'../../assets/images/course_down.png',
        show:false,
        dataIndex:0
      })
    }
    
   },
   //综合排序切换
   totalOrder(){
     console.log('综合排序切换');
     this.setData({
      page:0,
      pageAdd:false
    })
     if(this.data.imgDown1 === '../../assets/images/course_down.png'){
      this.setData({
        imgDown1:'../../assets/images/course_up.png',
        dataIndex:1,
        show:true,
        imgDown:'../../assets/images/course_down.png',
      })
    }else{
      this.setData({
        imgDown1:'../../assets/images/course_down.png',
        dataIndex:1,
        show:false,
      })
    }
   },
   //选择paixu
   chooseSort(e){
     let sort = ''
     this.setData({
       page:0,
       pageAdd:false
     })
     if(e.currentTarget.dataset.index === '0'){
       this.setData({
         sortText:'综合排序'
       })
       sort = ''
     }
     if(e.currentTarget.dataset.index === '1'){
      this.setData({
        sortText:'最新课程'
      })
      sort = 'time'
    }
    if(e.currentTarget.dataset.index === '2'){
      this.setData({
        sortText:'热门课程'
      })
      sort = 'hot'
    }
    this.setData({
      sortIndex:e.currentTarget.dataset.index,
      show:false,
      imgDown1:'../../assets/images/course_down.png'
    })
    this.getList('','vod',sort,this.data.page)
   },
   login(){
    wx.showModal({
      title: '提示',
      content: '是否立即前往登录！',
      success (res) {
        if (res.confirm) {
          console.log('用户点击确定')
          wx.switchTab({
            url: '/pages/mine/index',
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
   },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('切换tab触发onload',options)
    let  categoryId  = 0
    this.setData({
      selected:0,
      isLogin:wx.getStorageSync('isLogin')
    })
    if(this.data.isLogin){
      if(options.id){
        categoryId = options.id
        this.setData({
          categoryId:categoryId
        })
      }
      this.getSearch();
    }
    console.log('isLogin',this.data.isLogin)
   

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
   //解决tabbar渲染延迟问题
   onShow(){
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        active: 1
      })
    }
  },
//跳转课程详情
toLesson(e){
  console.log('id',e.currentTarget.dataset.id)
  console.log('index',e.currentTarget.dataset.index)
  wx.navigateTo({
    url: '/course/courseInfo/index?id='+e.currentTarget.dataset.id + '&isVip='+this.data.listData[e.currentTarget.dataset.index].isVip,
  })
},
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
   //搜索输入框
   toSearch(e) {
    wx.navigateTo({
      url: '/course/searchCourse/index',
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
      this.setData({
        page:this.data.page +1,
        pageAdd:true
      })
      console.log('上拉加载',this.data.page)
      if(!this.data.categoryId){
        this.getList('all','vod','',this.data.page)
      }else{
        this.getList(this.data.categoryId,'vod','',this.data.page)
      }
     
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})