const app = getApp();
Component({
  data: {
    active:0,
    selected: 0,
    color: "#7A7E83",
    selectedColor: "#3cc51f",
    list: [
      {
        "pagePath": "/pages/index/index",
        "iconPath": "/assets/images/home_first.png",
        "selectedIconPath": "/assets/images/home_first_hl.png",
        "text": "首页"
      },
      {
        "pagePath": "/pages/course/index",
        "iconPath": "/assets/images/home_course.png",
        "selectedIconPath": "/assets/images/home_course_hl.png",
        "text": "课程"
      },
      {
        "pagePath": "/pages/mine/index",
        "iconPath": "/assets/images/home_mine.png",
        "selectedIconPath": "/assets/images/home_mine_hl.png",
        "text": "我的"
      }
  ]
  },
  attached() {
  },
  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset
      const url = data.path
      console.log(url+data.index)
      getApp().globalData.activeIndex = data.index
      wx.switchTab({url})
      this.setData({
        selected: data.index
      })
    },
    onChange(event) {
      const index = event.detail
      wx.switchTab({
        url: this.data.list[index].pagePath,
        success: () => {
          this.setData({
            active: index
          });
          console.log(this.data.active)
        },
        fail:(e)=>{
          console.log(e)
        }
      })
      console.log('切换tab事件');
      //获取推荐位
      if(index === 0){
        this.getrecommend();
      }
      //获取课程筛选数据结构
      if(index === 1){
        // this.getSearch()
      }
      //获取学习进度和我的套餐
      if(index === 2){
        
      }
        
     
    },
    //获取推荐位
    getrecommend(){
      let that = this;
        var data = {
          mac:wx.getStorageSync('uuid'),
          menuId:'vod'
        }
        app.getRequest('/boapi/v1/menu/recommend', data,  (res) => {
          if (!res.error) {
          console.log('menu',res)
          }
        }, (err) => { })
    },
    
    //获取学习进度和我的套餐
    getLearn(){

    },
    init(){
      var iconPath = "list[0].iconPath";//先用一个变量，把(info[0].gMoney)用字符串拼接起来
      var text = "list[0].text";
      // var position = "list[" + e.currentTarget.id+ "].position";
      // this.setData({
      //     [iconPath]:'/assets/images/home_course.png',
      //     [text]:'我的'
      // })
    },


  },
  ready() {
		this.init();
	},
})