// pages/orderConfirm/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    productName:'',
    sum:'',
    contentId:'',
    productId:'',
    list:[]
  },
   confirm(){
    
   },
   //确认下单
   confirmOrder(){
    let that = this;
    var data = {
      contentId:this.data.contentId,
      productId:this.data.productId,
      mac:wx.getStorageSync('uuid'),
      payType:'wechat',
      amount:this.data.sum,
      payParam:'JSAPI'
    }
    app.getRequest('/boapi/v1/product/purchase', data,  (res) => {
      if (!res.error) {
      console.log('list',res)
      app.globalData.orderList.push(res);
      console.log('orderList',app.globalData.orderList)
      wx.setStorageSync('orderList', app.globalData.orderList)
      wx.navigateTo({
        url: '/order/orderInfo/index?status=待支付' + '&id='+res.orderId,
      })
      }
    }, (err) => { })
   },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      productName:options.productName,
      sum:options.price,
      productId:options.productId,
      contentId:options.contentId
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})