// pages/myorder/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    list:[],
    page:0,
    pageAdd:'',
    type:''

  },
  onChange(event) {
   let title=['','wait','success','cancel']
   this.setData({
     type:title[event.detail.name],
     list:[]
   })
   console.log('type',this.data.type)
   this.getList(this.data.type,0)
  },
  toInfo(e){
    console.log('22',e)
    wx.navigateTo({
      url: '/order/orderInfo/index?status='+e.currentTarget.dataset.type+'&id='+e.currentTarget.dataset.id,
    })
  },
  toInfo1(e){
    console.log('22')
    wx.navigateTo({
      url: '/order/orderInfo/index?status='+e.currentTarget.dataset.type+'&id='+e.currentTarget.dataset.id,
    })
  },
  toInfo2(e){
    console.log('22')
    wx.navigateTo({
      url: '/order/orderInfo/index?status='+e.currentTarget.dataset.type+'&id='+e.currentTarget.dataset.id,
    })
  },
   //替换时间戳
   transTime(unixtime) {
    if(unixtime.length === 10){
      var dateTime = new Date(parseInt(unixtime) * 1000)
    }else{
      var dateTime = new Date(parseInt(unixtime))
    }
      var year = dateTime.getFullYear();
      var month = dateTime.getMonth() + 1;
      if(month < 10){
        month  = '0'+month
      }else{
        month = month
      }
      var day = dateTime.getDate();
      if(day < 10){
        day  = '0'+day
      }else{
        day = day
      }
      var hour = dateTime.getHours();
      if(hour < 10){
        hour  = '0'+hour
      }else{
        hour = hour
      }
      var minute = dateTime.getMinutes();
      if(minute < 10){
        minute  = '0'+minute
      }else{
        minute = minute
      }
      var second = dateTime.getSeconds();
      if(second < 10){
        second  = '0'+second
      }else{
        second = second
      }
      var now = new Date();
      var now_new = Date.parse(now.toDateString());
      var milliseconds = now_new - dateTime;
      var timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
      return timeSpanStr;
    },
  //获取订单列表
  getList(status,page){
    let that = this;
    var data = {
      mac:wx.getStorageSync('uuid'),
      pageSize:15,
      page:page,
      orderStatus:status
    }
    app.getRequest('/boapi/v1/product/orderEdu', data, (res) => {
      console.log('订单列表',res)
      res.orders.forEach((item,index)=>{
        res.orders[index].orderTime = this.transTime(res.orders[index].orderTime)
        if(res.orders[index].orderStatus === 'cancel'){
          res.orders[index].orderStatus = '已取消'
        }
        if(res.orders[index].orderStatus === 'wait'){
          res.orders[index].orderStatus = '待支付'
        }
        if(res.orders[index].orderStatus === 'success'){
          res.orders[index].orderStatus = '已完成'
        }
      })
      let content = res.orders
      if(content.length === 0){
        wx.showToast({
          title: '已加载全部订单',
          icon: 'none'
        })
      }
      if(this.data.pageAdd){
        if(content){
          that.setData({
            list:this.data.list.concat(content)
          })
        }
        if (content.length>0){
          // this.data.page++
        }else{
          this.data.pageAdd=''
        }
        
      }else{
        this.setData({
          list:res.orders
        })
       
      }
      console.log('订单显示数据',this.data.list)
      
    }, (err) => { })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      active:0,
      type:''
    })
    this.getList(this.data.type,0)
  

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({
      page:this.data.page +1,
      pageAdd:true
    })
    console.log('上拉加载',this.data.page)
    this.getList(this.data.type,this.data.page)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})