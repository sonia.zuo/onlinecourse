// pages/orderinfo/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    status:'',
    productName:'',
    sum:'',
    orderInfo:'',
    time:'',
    orderId:'',
    countDown:'00:00',
    startTime:'',
    endTime:'',
    timer:'',
    timeStamp:''
  },
  //替换时间戳
  transTime(unixtime) {
    if(unixtime.length === 10){
      var dateTime = new Date(parseInt(unixtime) * 1000)
    }else{
      var dateTime = new Date(parseInt(unixtime))
    }
      var year = dateTime.getFullYear();
      var month = dateTime.getMonth() + 1;
      if(month < 10){
        month  = '0'+month
      }else{
        month = month
      }
      var day = dateTime.getDate();
      if(day < 10){
        day  = '0'+day
      }else{
        day = day
      }
      var hour = dateTime.getHours();
      if(hour < 10){
        hour  = '0'+hour
      }else{
        hour = hour
      }
      var minute = dateTime.getMinutes();
      if(minute < 10){
        minute  = '0'+minute
      }else{
        minute = minute
      }
      var second = dateTime.getSeconds();
      if(second < 10){
        second  = '0'+second
      }else{
        second = second
      }
      var now = new Date();
      var now_new = Date.parse(now.toDateString());
      var milliseconds = now_new - dateTime;
      var timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
      return timeSpanStr;
    },
    //查询支付结果
    getResult(id){
      let that = this;
      var data = {
        mac:wx.getStorageSync('uuid'),
        orderId: id
      }
      app.getRequest('/boapi/v1/wxpay/miniprogram/orderquery', data, (res) => {
        console.log('订单列表',res)
        if(res.payResult.orderStatus === 'SUCCESS'){
          wx.navigateTo({
            url: '/order/payResult/index?id='+that.data.orderId,
          })
        }else{
          wx.showModal({
            title: '提示',
            content: '稍后进入我的订单页核实订单状态，不要重复发起支付!',
            success (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        
        }
      }, (err) => { })
    },
  //获取订单详情
  getInfo(id){
    var data = {
      mac:wx.getStorageSync('uuid'),
      orderId:id,
      pageSize:'28',
      page:'0'
    }
    app.getRequest('/boapi/v1/product/order/detailEdu', data, (res) => {
      console.log('订单列表',res)
      let orderList = res
      this.setData({
        productName:orderList.productName,
        sum: orderList.fee,
        time:this.transTime(orderList.orderTime),
        orderId:orderList.orderId
      })
      if(this.data.status === '待支付'){
        let time =  orderList.orderTime; 
        let endTime = time + 30 * 60000
        this.setData({
          startTime:time,
          endTime: endTime
        })
        // let list = wx.getStorageSync('orderList')
        let list = app.globalData.orderList;
        if(list.length === 0){
          list = wx.getStorageSync('orderList')
        }
        console.log('list',list)
        list.forEach((item,index)=>{
          console.log('item',item.orderId)
          if(list[index].orderId === orderList.orderId){
            console.log('item',item)
            this.setData({
              orderInfo:item
            })
          }
        })
        console.log('orderinfo',this.data.orderInfo)
        this.count()

      }
      console.log('status',this.data.orderInfo)
      console.log('time',this.data.time)
    }, (err) => { })
  },
  //定时器
  count() {
    var that = this
    // var starttime = '2019/07/30 09:04:19'
    // var start = new Date(starttime.replace(/-/g, "/")).getTime()
    // var endTime = start + 15 * 60000
    // var date = new Date(); //现在时间
    // var now = date.getTime(); //现在时间戳
    var now = Date.parse(new Date());  
    // timestamp = timestamp / 1000;  
    // console.log("当前时间戳为：" + now); 
    var allTime = this.data.endTime - now
    var m, s;
    if (allTime > 0) {
      m = Math.floor(allTime / 1000 / 60 % 60);
      s = Math.floor(allTime / 1000 % 60);
      if(s === 0 || s < 10){
        s = '0'+s 
      }
      if(m === 0 || m < 10){
        m = '0'+m
      }
      that.setData({
        countDown: m + "分" + s +"秒",
      })
      console.log('倒计时',this.data.countDown)
      var time = setTimeout(that.count, 1000);
      this.setData({
        timer :time
      })
    } else {
      console.log('已截止')
      that.setData({
        countdown: '00:00'
      })
      this.cancel()
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      if(options.status){
        if(options.id ){
          this.setData({
            status:options.status,
            orderId:options.id,
          })
          this.getInfo(options.id)
        }
      }
      console.log('status',this.data.status)
     
  },
  cancel1(){
    let that = this;
    wx.showModal({
      title: '订单取消',
      content: '您确定要取消订单吗？',
      cancelText:'我再想想',
      confirmText:'确定',
      success (res) {
        if (res.confirm) {
          console.log('用户点击确定')
          var data = {
            mac:wx.getStorageSync('uuid'),
            orderId:that.data.orderId,
          }
          app.getRequest('/boapi/v1/order/cancel', data, (res) => {
            console.log('订单取消',res)
           clearInterval(that.data.timer);
           wx.navigateBack({
            delta: 1
          })
            
          }, (err) => { })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
   
  },
  cancel(){
    var data = {
      mac:wx.getStorageSync('uuid'),
      orderId:this.data.orderId,
    }
    app.getRequest('/boapi/v1/order/cancel', data, (res) => {
      console.log('订单取消',res)
     clearInterval(this.data.timer);
     wx.showModal({
      title: '提示',
      content: '您的支付时间已超时，订单已取消!',
      showCancel:false,
      success (res) {
        if (res.confirm) {
          console.log('用户点击确定')
          wx.switchTab({
            url: '/pages/mine/index',
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
      
    }, (err) => { })
  },
  pay(){
      let that = this;
      console.log('时间戳',this.data.orderInfo.timeStamp)
      console.log('nonceStr',this.data.orderInfo.nonceStr)
      console.log('package',this.data.orderInfo.prepayId)
      console.log('signType',this.data.orderInfo.signType)
      console.log('paySign',this.data.orderInfo.paySign)
    wx.requestPayment({
      timeStamp: this.data.orderInfo.timeStamp,
      nonceStr: this.data.orderInfo.nonceStr,
      package: this.data.orderInfo.prepayId,
      signType: this.data.orderInfo.signType,
      paySign: this.data.orderInfo.paySign,
      success (res) { 
        console.log('支付成功',res)
        clearInterval(that.data.timer);
        that.getResult(that.data.orderInfo.orderId)
       
      },
      fail (res) { 
        console.log('支付失败',res)
        // clearInterval(that.data.timer);
        wx.showModal({
          title: '提示',
          content: '支付失败！',
          success (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      
        
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(this.data.timer);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})