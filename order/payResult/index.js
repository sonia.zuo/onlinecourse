// pages/payResult/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
     id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
         id:options.id
    })
  },
   //返回
   back(){
    wx.navigateBack({
      delta: 4
    })
   },
   getResult(id){
    let that = this;
    var data = {
      mac:wx.getStorageSync('uuid'),
      orderId: '1298135473612525568'
    }
    app.getRequest('/boapi/v1/wxpay/miniprogram/orderquery', data, (res) => {
      console.log('订单列表',res)
      if(res.payResult.orderStatus === 'success'){
        wx.navigateTo({
          url: '/order/payResult/index?id='+that.data.orderId,
        })
      }else{
        wx.showModal({
          title: '提示',
          content: '稍后进入订单管理页核实订单状态，不要重复发起支付!',
          success (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      
      }
    }, (err) => { })
  },
   //查看订单
   check(){
     wx.navigateTo({
       url: '/order/orderInfo/index?status=已完成'+'&id='+this.data.id,
     })
   },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})