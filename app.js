//app.js
import {
  VeevlinkLogin,
  VeevlinkIslogin,
  VeevlinkClient,
  VeevlinkData,
  getUUID,
  randomWord,
  createSignUrl,
  getRequest,
  createSign,
} from './utils/base'
import {
  formatTime,
} from './utils/index'
App({
  VeevlinkClient,
  VeevlinkData,
  VeevlinkIslogin,
  VeevlinkLogin,
  getUUID,
  randomWord,
  createSignUrl,
  getRequest,
  createSign,
  formatTime,
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
    //获取设备和系统信息
    wx.getSystemInfo({
      success: res=> {
        console.log(res)
        console.log(res.model)
        console.log(res.pixelRatio)
        console.log(res.windowWidth)
        console.log(res.windowHeight)
        console.log(res.language)
        console.log(res.version)
        console.log(res.platform)
        if (res.platform == 'android'){
          this.globalData.deviceType = 'androidmobile'
        }
        if (res.platform == "ios") {   
          this.globalData.deviceType = 'iphone'
        }
        if (res.platform == "pc") {   
          console.log('pc设备')
          this.globalData.deviceType = 'pc'
        }else{
          this.globalData.deviceType = 'pc'
          
        }
       
      }
    })
  },
  globalData: {
    orderList:[],
    menuList:'',
    deviceType:'',
    openid:'',
    token:'',
    activeIndex:0,
    targetPage: '',
    userInfo: null,
    formal: false,
    // http://10.10.63.202:8999
    // https://edu.xmediatv.cn
    // http://10.10.122.38:8868
    // http://10.10.63.202:8868
    url: 'https://edu.xmediatv.cn',
    appid: 'wxf85606512ae21592',
    iv: '',
    data: '',
  }
})



