//index.js
//获取应用实例
const app = getApp()

Page({
  onShareAppMessage() {
    return {
      title: 'swiper',
      path: 'page/component/pages/swiper/swiper'
    }
  },
  data: {
    rightText:'换一换',
    more:'更多',
    imgList:[],
    menuList:[],
    recommendList:[],
    swiperCurrent:0,
    value:'',
    active:0,
    list:'11',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    background: ['demo-text-1', 'demo-text-2', 'demo-text-3'],
    indicatorDots: false,
    vertical: false,
    autoplay: true,
    interval: 8000,
    duration: 500,
    circular:true,
    isShow:false
  },
  //点击跳转详情
  toDetail(e){
    console.log('跳转',e)
    if(wx.getStorageSync('phoneNumber')){
      wx.navigateTo({
        url: '/course/courseInfo/index?id='+ this.data.imgList[e.currentTarget.dataset.index].contentId +'&isVip='+this.data.imgList[e.currentTarget.dataset.index].isVip,
      })
      
    }else{
      wx.showModal({
        title: '提示',
        content: '请先登录，再进行操作！',
        success (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })

    }
   
  },
  //轮播图片
    swiperChange(e) {
      let current = e.detail.current;
      // console.log(current, '轮播图')
      let that = this;
      that.setData({
        swiperCurrent: current,
      })
 
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  getPhoneNumber (e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
    //调用接口解密
    // app.VeevlinkClient(()=>{
      
    // })
  },
   //解决tabbar渲染延迟问题
   onShow(){
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        active: 0
      })
    }
  },
  //搜索输入框
  toSearch(e) {
    if(wx.getStorageSync('phoneNumber')){
      wx.navigateTo({
        url: '/course/searchCourse/index',
      })

    }else{
      wx.showModal({
        title: '提示',
        content: '请先登录，再进行操作！',
        success (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      
    }
   
  },
  //更多课程
  more(e){
    console.log('e',e)
    let id = e.currentTarget.dataset.id;
    console.log('更多');
    if(wx.getStorageSync('phoneNumber')){
      wx.reLaunch({
        url: '/pages/course/index?id='+id
      })

    }else{
      wx.showModal({
        title: '提示',
        content: '请先登录，再进行操作！',
        success (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      
    }
   
  },
  //跳转课程详情
  toLesson(e){
    console.log(e)
    console.log('id',e.currentTarget.dataset.id)
    console.log('isvip',e.currentTarget.dataset.vip)
    if(wx.getStorageSync('phoneNumber')){
      wx.navigateTo({
        url: '/course/courseInfo/index?id='+e.currentTarget.dataset.id+'&isVip='+e.currentTarget.dataset.vip,
      })

    }else{
      wx.showModal({
        title: '提示',
        content: '请先登录，再进行操作！',
        success (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      
    }
    
  },
  onSearch() {
    console.log('搜索' + this.data.value);
  },
  //换一换
  change(){
    console.log('换一换');
    if(wx.getStorageSync('phoneNumber')){
      var data = {
        type:'vod',
        pageSize: 4,
        page:0
      }
      app.getRequest('/boapi/v1/home/recommend', data,  (res) => {
        if (!res.error) {
         console.log('recommend',res)
         let list = [];
         this.data.recommendList.forEach((item,index)=>{
           if(item.title === '为你推荐'){
            this.data.recommendList[index].rContent = res.contents
           }
  
         })
         list = this.data.recommendList
         this.setData({
          recommendList:list
         })
  
        }
      }, (err) => { })

    }else{
      wx.showModal({
        title: '提示',
        content: '请先登录，再进行操作！',
        success (res) {
          if (res.confirm) {
            console.log('用户点击确定')
            wx.switchTab({
              url: '/pages/mine/index',
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      
    }
  
  },
  //获取推荐位
  getRecomend(){
    let that = this;
    let menuId = app.globalData.menuList[0].menuId.toString()
    var data = {
      mac:wx.getStorageSync('uuid'),
      menuId:menuId
    }
    app.getRequest('/boapi/v1/menu/recommend', data,  (res) => {
      if (!res.error) {
       console.log('menu',res.menus);
       this.setData({
        recommendList:res.menus[0].recommend
       })
       let list = [];
       this.data.recommendList.forEach((item,index)=>{
         if(item.style === 'Banner'){
           item.rContent.forEach((item1,index1)=>{
            list.push({
              url:item1.poster,
              contentId: item1.url,
              isVip:item1.isVip
            })
           })
         }
       })
       this.setData({
         imgList:list,
         isShow:true
       })
       console.log('轮播图',this.data.imgList)
       console.log('recommendList',this.data.recommendList);
      //  let list = [];
      //  res.menus[0].recommend.forEach((item,index)=>{
              //  list.push({
              //   categoryId: item.categoryId,
              //   type:res.menus[0].type,
              //   sort:item.sort,
              //   title:item.title,
              //  })
              // console.log('list',list)
              //  that.getContent(list[index])
      //  })
       

      }
    }, (err) => { })
  },
  //获取菜单id
    getMenu(){
        let that = this;
        var data = {
          menuId:'0',
          language:'zh_CN',
          mac:wx.getStorageSync('uuid'),
        };
        console.log('menudata',data)
        app.getRequest('/boapi/v1/menu/list', data,  (res) => {
          if (!res.error) {
           console.log('menu',res)
           app.globalData.menuList = res.menus
           console.log('menulist',app.globalData.menuList)
           that.getRecomend()
          
          }
        }, (err) => { })
      },
    

  //跳转播放页面
  watch(){
    wx.navigateTo({
      url: '/pages/test/index',
      events: {
        // 为指定事件添加一个监听器，获取被打开页面传送到当前页面的数据
        backFromTargetPage: function (backData) {
          if (backData.from == 'page2') {
            // 监听到 page2 返回
            console.log('好啦，可以在这里写你的逻辑了', backData)
            // t.setData({
            //   isBackFromPage2: !0
            // })
          }
        }
      }
    })
  },
  //首页适配
  // getInfo(){
  //   wx.getSystemInfo({
  //     success (res) {
  //       console.log(res.model)
  //       console.log(res.pixelRatio)
  //       console.log(res.windowWidth)
  //       console.log(res.windowHeight)
  //       console.log(res.language)
  //       console.log(res.version)
  //       console.log(res.platform)
  //     }
  //   })
  // },
  getInfo(){
    let that = this;
    wx.login({
      success (res) {
        if (res.code) {
          console.log(res)
          console.log('code',res.code);
          var data = {
            userAccessToken: res.code,
            username: res.code,
            loginType:'wechat-miniprogram'
          }
          app.getRequest('/boapi/v1/auth/login', data, (res) => {
            console.log('接口返回getinfo',res)
            if (res.resultCode === 0) {
                console.log('res',res)
                app.globalData.token = res.token;
                wx.setStorageSync('token', res.token)
                wx.setStorageSync('memberId', res.userInfo.memberId)
                console.log('tokenurl',app.globalData.token)
                wx.setStorageSync('isVip', res.userInfo.vip)
                that.getMenu();
                if(res.userInfo.userType === 'login'){
                  wx.setStorageSync('isLogin', true)
                }else{
                  wx.setStorageSync('isLogin', false)
                }
                
                
            }else{
              wx.showToast({
                title: '网络有点问题，请稍后重试！',
              })

            }
          }, (err) => { })
          
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
   },
  onLoad: function () {
    let that = this;
    console.log('token111',wx.getStorageSync('token'));
    this.getInfo();
    if(wx.getStorageSync('token')){
      this.getMenu();
    }else{
      wx.setStorageSync('userInfo', '')
      wx.setStorageSync('isLogin', false)
      // this.getInfo();
    }
    
  },
   //右上角分享功能
   onShareAppMessage: function (res) {
    var that = this;
    return {
      title: '教育云微课',
      path: '/pages/index/index?id=' + that.data.scratchId,
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },


})
