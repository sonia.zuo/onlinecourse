//获取应用实例
const app = getApp()
function getRandomColor () {
  const rgb = []
  for (let i = 0 ; i < 3; ++i){
    let color = Math.floor(Math.random() * 256).toString(16)
    color = color.length == 1 ? '0' + color : color
    rgb.push(color)
  }
  return '#' + rgb.join('')
}

Page({
  onReady: function (res) {
    this.videoContext = wx.createVideoContext('myVideo')
    var oDelList = app.globalData.delList;
        this.setData({
            list: this.data.delList
        })
  },
  inputValue: '',
    data: {
      delBtnWidth: 180,
      list: [],
      startX: "",
      arr: [
        {
          name:'name',
          content:'content',
          status: true
        },
        {
          name: 'name',
          content: 'content',
          status: true
        }
   ],
     delList: [
        {
        txtStyle: "",
        icon: "/images/icon0.png",
        txt: "向左滑动可以删除"
        },
        {
        txtStyle: "",
        icon: "/images/icon6.png",
        txt: "微信小程序|联盟（wxapp-union.com）"
        },
        {
        txtStyle: "",
        icon: "/images/icon1.png",
        txt: "圣诞老人是爸爸，顺着烟囱往下爬，礼物塞满圣诞袜，平安糖果一大把"
        },
        {
        txtStyle: "",
        icon: "/images/icon2.png",
        txt: "圣诞到来，元旦还会远吗？在圣诞这个日子里"
        },
        {
        txtStyle: "",
        icon: "/images/icon3.png",
        txt: "圣诞节(Christmas或Cristo Messa ),译名为“基督弥撒”。"
        },
        {
        txtStyle: "",
        icon: "/images/icon4.png",
        txt: "一年一度的圣诞节即将到来,姑娘们也纷纷开始跑趴了吧!"
        },
         
        {
        txtStyle: "",
        icon: "/images/icon5.png",
        txt: "圣诞节(Christmas或Cristo Messa ),译名为“基督弥撒”。"
        },
        {
        txtStyle: "",
        icon: "/images/icon2.png",
        txt: "你的圣诞节礼物准备好了吗?"
        },
        {
        txtStyle: "",
        icon: "/images/icon3.png",
        txt: "一年一度的圣诞节即将到来,姑娘们也纷纷开始跑趴了吧!"
        },
        {
        txtStyle: "",
        icon: "/images/icon4.png",
        txt: "圣诞到来，元旦还会远吗？"
        },
        {
        txtStyle: "",
        icon: "/images/icon5.png",
        txt: "记下这一刻的心情"
        },
        ],
    currentTime:'',
    src: '',
    danmuList:
      [{
        text: '第 1s 出现的弹幕',
        color: '#ff0000',
        time: 1
      },
      {
        text: '第 3s 出现的弹幕',
        color: '#ff00ff',
        time: 3
      }],
      feed_style:{

        x:"",
        
        y:"",
        
        },
        //这个参数是定位使用的x ， y值
        // 仅在js里面传递的参数：
        screen:{
                width:"",
                height:""
        
        }, // 用于保存屏幕页面信息,上次的x值,上次的y值
        preX:'',
        preY:'',
        msg:'start',
        flag:false
    },
   
  
    touchM(e) {
      // 获得当前坐标
      this.currentX = e.touches[0].clientX;
      this.currentY = e.touches[0].clientY;
      //刚点击的坐标 - 最后停止的坐标 > 0 说明左滑
      const x = this.startX - this.currentX; //横向移动距离
      if (x > 10) {
        //向左滑是显示删除
        this.setData({
          //只让你当前滑动的这个元素的状态改变
          [`arr[${e.currentTarget.dataset.index}].status`]: false
        })
      } else if (x < -10) {
        //向右滑
        this.setData({
          [`arr[${e.currentTarget.dataset.index}].status`]: true
        })
      }
  
    },
    touchS(e) {
      // 获取手指点击的（X,Y）坐标
      // 获得起始坐标
      this.startX = e.touches[0].clientX;
      this.startY = e.touches[0].clientY;
      this.data.arr.forEach((item, index) => {
        if (index !== e.currentTarget.dataset.index) {
          item.status = true
          //遍历改变status值,让非当前点击下标的元素，不显示删除
        }
      })
      this.setData({
        //重新赋值，渲染到页面
        arr: this.data.arr
      })
    },
    del(val){
      let that = this
      let index = val.currentTarget.dataset.index
      wx.showModal({
        title: '提示',
        content: '确认删除吗',
        success(res) {
          if (res.confirm) {
            console.log('用户点击确定')
            let arr= that.data.arr
            arr.splice(index, 1)
            that.setData({
              arr: arr
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    },

  bindInputBlur: function(e) {
    this.inputValue = e.detail.value
  },
  // 视频时长更新
  timeupdate(e){
    console.log('视频时长',e)
    let duration = e.detail.duration
    let currentTime = e.detail.currentTime
    console.log(duration)
    console.log('时长',currentTime)
    this.setData({
      currentTime:currentTime
    })
    if (currentTime > duration - 2.925){ 
       // 停止视频
      wx.createVideoContext(this.data.indexCurrent, this).stop()
       // 暂停视频
      // wx.createVideoContext(this.data.indexCurrent, this).pause()
      console.log("可以暂停了")
    }
  },
  //视频暂停
  stop(e){
    console.log('暂停',e);
  },
  bindSendDanmu: function () {
    this.videoContext.sendDanmu({
      text: this.inputValue,
      color: getRandomColor()
    })
  },
  bindPlay: function() {
    this.videoContext.play()
  },
  bindPause: function() {
    this.videoContext.pause()
  },
  videoErrorCallback: function(e) {
    console.log('视频错误信息:')
    console.log(e.detail.errMsg)
  },
  bindPay(){
     wx.navigateTo({
       url: '/my/confirm/index',
     })
  },
  touchMoveChange(e){

    var tmpx = parseInt(e.touches[0].clientX);
    
    var tmpy = parseInt(e.touches[0].clientY);
    
    if (tmpx <= 0 || tmpy <= 0 || tmpx >= this.data.screen.width || tmpy >= this.data.screen.height ){
    
    }else{
    
    if (tmpx != this.data.preX || tmpy != this.data.preY ){
    
    console.log(e.touches[0].clientX, "-X-", e.touches[0].pageX)
    
    console.log(e.touches[0].clientY, "-Y-", e.touches[0].pageY)
    
    this.data.preX = tmpx
    
    this.data.preY = tmpy
    
    this.setData({
    
    feed_style: {
    
    x: tmpx - 50 + "px",
    
    y: tmpy - 50 + "px",
    
    }
    
    })
    
    }
    
    }
    
    },
  //销毁当前页面
  onUnload(){
    let that = this;
    console.log('销毁当前页面',this.currentTime)
    app.globalData.targetPageEventChannel = this.getOpenerEventChannel()
    // 建议>=400毫秒
    setTimeout(function () {
      var t = getApp().globalData.targetPageEventChannel
      t ? t.emit('backFromTargetPage', { from: 'page2' }) : console.log('not targetPage navigateBack, canceled')
    }, 400)
  },
  onLoad(){
    let that = this;
    wx.getSystemInfo({

      success: function (res) {
      
      console.log(res);
      
      console.log("platform",res.platform);
      
      console.log(res.model);
      
      // 可使用窗口宽度、高度
      
      console.log('height=' + res.windowHeight);
      
      console.log('width=' + res.windowWidth);
      
      // Math.ceil()
      
      if(res.platform == "android"){
      
      res.windowHeight = res.screenHeight;
      
      }
      
      // feed_style: {
      
      // x: res.windowWidth + "px",
      
      // y: res.windowHeight + "px"
      
      // },
      
      that.setData({
      
      screen:{
      
      width: res.windowWidth ,
      
      height: res.windowHeight ,
      
      pixelRatio: res.pixelRatio,
      
      ratio: res.windowWidth * res.pixelRatio/750
      
      }
      
      })
      
      
      // 计算主体部分高度,单位为px
      
      // that.setData({
      
      // second_height: res.windowHeight
      
      // })
      
      }
      
      })
  },
  //视频加载完元数据时触发
  bindloadedmetadata(e){
    console.log('视频加载完元数据时触发',e)

  },
  //seek
  bindSeek(){
    console.log('seek')
    this.videoContext.seek(10)
  },
   // 触发点击事件时执行的操作
   add(){
    var flag = [this.data.flag]
    console.log(flag)
    if(flag == 'false'){
      this.setData({
        msg:'stop',
        flag:true
      })
    }else{
      this.setData({
        msg:'start',
        flag:false
      })
    }
  },
})