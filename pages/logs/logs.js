//logs.js
const util = require('../../utils/util.js')

Page({
  data: {
    logs: [],
    msg:'start',
    flag:false,
    msg1:'start',
    flag:false,
    left:0,
    top:0,
    height:'',
    imglist:[
      'https://img.yzcdn.cn/vant/apple-1.jpg',
      'https://img.yzcdn.cn/vant/apple-2.jpg',
      'https://img.yzcdn.cn/vant/apple-3.jpg'
    ]

  },
   // 触发点击事件时执行的操作
   add(){
    var flag = [this.data.flag]
    console.log(flag)
    if(flag == 'false'){
      this.setData({
        msg:'stop',
        flag:true
      })
    }else{
      this.setData({
        msg:'start',
        flag:false
      })
    }
  },
  moveto(e){
    console.log('e',e)
    var left = e.detail.x;
    var top = e.detail.y;
    var index = e.currentTarget.dataset.index;
    this.setData({
      left:left,
      top:top
    })
    this.data.height = this.data.height - 175*index;
    console.log(this.data.height);
    if(this.data.top === this.data.height){
      wx.showToast({
        title: '删除成功！',
        icon: 'success',
        duration: 2000
      })
      var list = this.data.imglist;
      console.log(list);
      list.splice(index,1);
      console.log('list',list)
      this.setData({
        imglist:list
      })
      this.setData({
        left:0,
        top:0
      })
      console.log(index)
      console.log(this.data.imglist)
      console.log(this.data.top)
      
    }
   
    

  },
  onLoad: function () {
    let that = this;
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(log => {
        return util.formatTime(new Date(log))
      })
    })
    wx.getSystemInfo({
      success (res) {
        console.log(res.model)
        console.log(res.pixelRatio)
        console.log(res.windowWidth)
        console.log(res.windowHeight)
        console.log(res.language)
        console.log(res.version)
        console.log(res.platform)
        that.setData({
          height: res.windowHeight - 135
        })
        console.log('heihgt',that.data.height)
      }
    })
  }
})
