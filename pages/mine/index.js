// pages/mine/index.js
//获取应用实例
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:'',
    imgUser:'../../assets/images/mine_user.png',
    title:'',
    cumulativeLearn:0,
    learnTime:0,
    section:0,
    list:[],
    dataInfo:'',
    userInfo:'',
    totalDuration:0,
    studyDays:0,
    courseCounts:0,
    listData:[],
    isVip:''

  },
  //获取用户信息
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true,
      imgUser:e.detail.userInfo.avatarUrl,
      name: e.detail.userInfo.nickName,
      isVip: wx.getStorageSync('isVip')
    })
    wx.setStorageSync('userInfo', app.globalData.userInfo)
      this.getInfo()
  },
  //获取学习进度
  getProgress(){
    var data = {
      mac:wx.getStorageSync('uuid'),
      memberId:wx.getStorageSync('memberId')
    }
    app.getRequest('/boapi/v1/learn/progress', data, (res) => {
      console.log('接口返回progress',res)
      if (res.resultCode === 0) {
          console.log('res',res)
          this.setData({
            courseCounts:res.courseCounts,
            studyDays:res.studyDays,
            totalDuration:res.totalDuration
          })
      }else{
        wx.showToast({
          title: '网络有点问题，请稍后重试！',
        })

      }
    }, (err) => { })
  },
  //获取我的套餐包
  getProduct(){
    var data = {
      mac:wx.getStorageSync('uuid'),
    }
    app.getRequest('/boapi/v1/product/effectiveEdu', data, (res) => {
      console.log('effectiveEdu',res)
      this.setData({
        listData:res.products
      })
    }, (err) => { })
  },
  getData(){
    let that = this;
    wx.login({
      success (res) {
        if (res.code) {
          console.log(res)
          console.log('code',res.code);
          var data = {
            userAccessToken: res.code,
            username: res.code,
            loginType:'wechat-miniprogram'
          }
          app.getRequest('/boapi/v1/auth/login', data, (res) => {
            console.log('接口返回getinfo',res)
            if (res.resultCode === 0) {
                console.log('res',res)
                app.globalData.token = res.token;
                wx.setStorageSync('token', res.token)
                wx.setStorageSync('memberId', res.userInfo.memberId)
                console.log('tokenurl',app.globalData.token)
                wx.setStorageSync('isVip', res.userInfo.vip)
                that.setData({
                  isVip:wx.getStorageSync('isVip'),
                })
                that.getProgress()
                that.getProduct()
                if(res.userInfo.userType === 'login'){
                  wx.setStorageSync('isLogin', true)
                }else{
                  wx.setStorageSync('isLogin', false)
                }
                
                
            }else{
              wx.showToast({
                title: '网络有点问题，请稍后重试！',
              })

            }
          }, (err) => { })
          
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
   },
  //获取用户信息
  getInfo(){
    let that = this;
    wx.login({
      success (res) {
        if (res.code) {
          console.log(res)
          console.log('code',res.code);
          var data = {
            userAccessToken: res.code,
            username: res.code,
            loginType:'wechat-miniprogram'
          }
          app.getRequest('/boapi/v1/auth/login', data, (res) => {
            console.log('接口返回getinfo',res)
            if (res.resultCode === 0) {
                console.log('res',res)
                app.globalData.token = res.token;
                wx.setStorageSync('token', res.token)
                wx.setStorageSync('memberId', res.userInfo.memberId)
                console.log('tokenurl',app.globalData.token)
                wx.navigateTo({
                  url: '/my/confirm/index',
                })
                
            }else{
              wx.showToast({
                title: '网络有点问题，请稍后重试！',
              })

            }
          }, (err) => { })
          
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
   },
  //我的vip
  myvip(){
    let that = this;
    if(wx.getStorageSync('phoneNumber')){
      wx.navigateTo({
        url: '/my/vip/index?url='+that.data.imgUser +'&name='+that.data.name,
      })

    }else{
      wx.showModal({
        title: '提示',
        content: '请先登录再进行操作！',
        success (res) {
          if (res.confirm) {
            console.log('用户点击确定')
            if(wx.getStorageSync('userInfo')){
              wx.navigateTo({
                url: '/my/confirm/index',
              })
            }
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
    
  },
    //开通vip
    openVip(){
      wx.navigateTo({
        url: '/my/openVip/index?type=mine'+'&id='+'',
      })
    },
    //跳转我的套餐包
    toPackage(e){
      console.log('e',e.currentTarget.dataset.index)
      wx.navigateTo({
        url: '/my/myPackage/index?id='+e.currentTarget.dataset.index+'&name='+e.currentTarget.dataset.name
      })
    },
    //发现课程
    toLesson(){
      console.log('tab跳转');
      if(wx.getStorageSync('phoneNumber')){
        wx.switchTab({
          url: '/pages/course/index'
        })

      }else{
        wx.showModal({
          title: '提示',
          content: '请先登录再进行操作！',
          success (res) {
            if (res.confirm) {
              console.log('用户点击确定')
              if(wx.getStorageSync('userInfo')){
                wx.navigateTo({
                  url: '/my/confirm/index',
                })
              }
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
      
    },
    //观看历史
    toHistory(){
      if(wx.getStorageSync('phoneNumber')){
        wx.navigateTo({
          url: '/my/history/index',
        })
      }else{
        wx.showModal({
          title: '提示',
          content: '请先登录再进行操作！',
          success (res) {
            if (res.confirm) {
              console.log('用户点击确定')
              if(wx.getStorageSync('userInfo')){
                wx.navigateTo({
                  url: '/my/confirm/index',
                })
              }
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    },
    //我的收藏
    myCollection(){
      if(wx.getStorageSync('phoneNumber')){
        wx.navigateTo({
          url: '/my/collect/index',
        })
      }else{
        wx.showModal({
          title: '提示',
          content: '请先登录再进行操作！',
          success (res) {
            if (res.confirm) {
              console.log('用户点击确定')
              if(wx.getStorageSync('userInfo')){
                wx.navigateTo({
                  url: '/my/confirm/index',
                })
              }
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
      
    },
    //我的订单
    myOrder(){
      if(wx.getStorageSync('phoneNumber')){
        wx.navigateTo({
          url: '/order/myOrder/index',
        })

      }else{
        wx.showModal({
          title: '提示',
          content: '请先登录再进行操作！',
          success (res) {
            if (res.confirm) {
              console.log('用户点击确定')
              if(wx.getStorageSync('userInfo')){
                wx.navigateTo({
                  url: '/my/confirm/index',
                })
              }
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      
      }
     
      
    },
    //联系我们
    contact(){
      if(wx.getStorageSync('phoneNumber')){
        wx.showModal({
          title: '',
          content: '020-38934377',
          confirmText:'呼叫',
          success (res) {
            if (res.confirm) {
              console.log('用户点击确定')
              wx.makePhoneCall({
                phoneNumber: '020-38934377' //仅为示例，并非真实的电话号码
              })
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        

      }else{
        wx.showModal({
          title: '提示',
          content: '请先登录再进行操作！',
          success (res) {
            if (res.confirm) {
              console.log('用户点击确定')
              if(wx.getStorageSync('userInfo')){
                wx.navigateTo({
                  url: '/my/confirm/index',
                })
              }
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })

      }
     


    },
    //获取手机号
    getPhoneNumber (e) {
      let that = this;
      console.log(e.detail.errMsg)
      console.log(e.detail.iv)
      console.log(e.detail.encryptedData)
      //调用接口解密
      // app.VeevlinkClient(()=>{
        
      // })
     

    },
    

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    if(wx.getStorageSync('phoneNumber') && wx.getStorageSync('isLogin')){
      wx.getUserInfo({
        success: function(res) {
          console.log('res',res);
          var userInfo = res.userInfo
          var nickName = userInfo.nickName
          var avatarUrl = userInfo.avatarUrl
          that.setData({
            imgUser:userInfo.avatarUrl,
            name: userInfo.nickName,
          })
          wx.setStorageSync('userInfo', userInfo)
          var gender = userInfo.gender //性别 0：未知、1：男、2：女
          var province = userInfo.province
          var city = userInfo.city
          var country = userInfo.country
        }
      })
      this.setData({
        isVip:wx.getStorageSync('isVip'),
        listData:[],
        name:''
      })
      if(wx.getStorageSync('userInfo')){
        let userInfo  = wx.getStorageSync('userInfo')
        that.setData({
          imgUser:userInfo.avatarUrl,
          name: userInfo.nickName,
        })
        that.getProgress()
        that.getProduct()
      }
      
    }
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
 //解决tabbar渲染延迟问题
 onShow(){
  if (typeof this.getTabBar === 'function' &&
    this.getTabBar()) {
    this.getTabBar().setData({
      active: 2
    })
  }
},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('下拉')
    this.getData()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('上拉')
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})