const app = getApp()
let listData = []
Page({
	data: {
		isIphoneX: app.globalData.isIphoneX,
		listData: [],
		size: 2,
		extraNodes: [
			{
				type: "destBefore",
				dragId: "destBefore0",
				destKey: 0,
				slot: "before",
				fixed: true
			},
			{
				type: "destAfter",
				dragId: "destAfter0",
				destKey: 0,
				slot: "after",
				fixed: true
			},
			{
				type: "after",
				dragId: "plus",
				slot: "plus",
				fixed: true
			}
		],
		pageMetaScrollTop: 0,
		scrollTop: 0
	},
	sortEnd(e) {
		console.log("sortEnd", e.detail.listData)
		this.setData({
			listData: e.detail.listData
		});
	},
	change(e) {
		console.log("change", e.detail.listData)
	},
	sizeChange(e) {
		wx.pageScrollTo({scrollTop: 0})
		this.setData({
			size: e.detail.value
		});
		this.drag.init();
	},
	itemClick(e) {
		console.log(e);
	},
	toggleFixed(e) {
		let key = e.currentTarget.dataset.key;

		let {listData} = this.data;

		listData[key].fixed = !listData[key].fixed

		this.setData({
			listData: listData
		});

		this.drag.init();
	},
	add(e) {
		let listData = this.data.listData;
		listData.push({
			dragId: `item${listData.length}`,
			title: "这个绝望的世界没有存在的价值，所剩的只有痛楚",
			description: "思念、愿望什么的都是一场空，被这种虚幻的东西绊住脚，什么都做不到",
			images: "/assets/image/swipe/1.png",
			fixed: false
		});
		setTimeout(() => {
			this.setData({
				listData,
			});
			this.drag.init();
		}, 300)

	},
	scroll(e) {
		this.setData({
			pageMetaScrollTop: e.detail.scrollTop
		})
	},
	// 页面滚动
	onPageScroll(e) {
		this.setData({
			scrollTop: e.scrollTop
		});
	},
	//获取收藏列表
	getList(){
		var data = {
			mac:wx.getStorageSync('uuid'),
			type:'vod',
			pageSize:15,
			page:0
		}
		app.getRequest('/boapi/v1/favorites/list', data,  (res) => {
			if (!res.error) {
			console.log('collgetListctLesson',res)
			let list = [];
			res.contents.forEach((item,index)=>{
				list.push({
					dragId: 'item'+index,
					title: item.name,
					description: "",
					images: item.poster,
					fixed: false,
					episodeTotal:item.episodeTotal,
					contentId: item.contentId,
					isVip:item.isVip
				})
			})
			listData = list
			this.setData({
				listData: listData
			});
			console.log('listData',this.data.listData)
			this.drag.init();
			}
		}, (err) => { })

	},
	opencourse(){
    wx.switchTab({
      url: '/pages/course/index',
    })
  },
	onLoad() {
		this.drag = this.selectComponent('#drag');
		// 模仿异步加载数据
		setTimeout(() => {
			this.setData({
				listData: []
			});
			this.getList()
			this.drag.init();
		}, 100)
		
	}
})
