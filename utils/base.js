/**
 * Created by Bella on 2020/7/14.
 */
var zhmd5 = require('md5.js')
export function VeevlinkIslogin (success, error, close) {
  let num = 0;
  login = () => {
    console.log('login', wx.getStorageSync('openid' + this.globalData.appid));
    // 判断有没有用户信息有的话不掉接口
    wx.login({
      success: (res) => {
        if (wx.getStorageSync('openid' + this.globalData.appid)) {
          wx.request({
            url: this.globalData.url + '/VeevlinkApplication/AppUserQuery.aspx',
            method: 'POST',
            data: {
              appid: this.globalData.appid,
              openId: wx.getStorageSync('openid' + this.globalData.appid)
            },
            header: {
              'Content-Type': 'application-json'

            },
            success: (res) => {
              console.log('resdata', res.data);
              wx.setStorageSync('information' + this.globalData.appid, res.data);
              success(wx.getStorageSync('openid' + this.globalData.appid));
            },
            fail: (err) => {
              error(this.globalData.appid)
            }
          });
        } else {
          wx.request({
            url: this.globalData.url + '/VeevlinkApplication/AppOpenIdQuery.aspx',
            data: {
              appid: this.globalData.appid,
              code: res.code
            },
            header: {
              'Content-Type': 'application-json'
            },
            success: (res2) => {
              console.log('3');
              console.log(res2);
              if (!res2.data) {
                close('openid获取失败');
                wx.hideLoading();
                return;
              }
              wx.setStorageSync('openid' + this.globalData.appid, res2.data);
              login();
            },
            fail: (err) => {
              wx.hideLoading();
              close('网络出错!请稍后重试')
            }
          })
        }
      },
      fail: (res) => {
        // console.log('客户拒绝授权');
        // login();
        wx.hideLoading();
        close('授权失败!请退出重试')
      },
    });

  }
  login();
}



export function VeevlinkLogin(data, iv, type, success, error, orderPramas) {
  console.log('iv', iv);
  console.log('data', data);
  this.globalData.iv = iv;
  this.globalData.data = data;
  // 用户登录获取授权信息
  wx.showLoading({
    title: '加载中',
  });
  wx.login({
    success: (res) => {
      console.log('VeevlinkLogin', res);
      console.log("code:" + res.code);
      wx.setStorageSync('code', res.code);
      console.log('12313');
      let Code = res.code;
      // 发送 res.code 到后台换取 openId, sessionKey, unionId
      if (res) {
        wx.request({
          url: this.globalData.url + '/VeevlinkApplication/AppOpenIdQuery.aspx',
          data: {
            appid: this.globalData.appid,
            code: res.code
          },
          header: {
            'Content-Type': 'application-json'
          },
          success: (res) => {
            console.log('res', res);
            console.log("openid===" + res.data)
            let OpenId = res.data;
            // 存openid
            wx.setStorageSync('openid' + this.globalData.appid, res.data);
            if (type === 'phone') {
              console.log('phone');
              wx.checkSession({
                success(data) {
                  //session_key 未过期，并且在本生命周期一直有效
                  console.log("su", data)
                  wx.request({
                    url: this.globalData.url + '/VeevlinkApplication/AppTelphone.aspx',
                    data: {
                      appid: this.globalData.appid,
                      openId: OpenId,
                      //code: Code,
                      encryptedData: this.globalData.data,
                      vi: this.globalData.iv
                    },
                    header: {
                      'Content-Type': 'application-json'

                    },
                    success: (res) => {
                      console.log('获取信息phone', res.data);
                      // wxe3786e0f1c17f2f9
                      wx.setStorageSync('phone', res.data.phoneNumber);

                      wx.hideLoading();
                      let timestamp = Date.parse(new Date());
                      wx.setStorageSync('timestamp' + this.globalData.appid, timestamp);
                      console.log('获取信息成功');
                      success();
                    }
                  });
                },
                fail: (data) => {
                  // session_key 已经失效，需要重新执行登录流程
                  console.log("dai", data)
                  VeevlinkLogin(this.globalData.data, this.globalData.iv);
                  //wx.login() //重新登录
                }
              })

            }
            if (type === 'confirm') {
              console.log('confirm');
              // 获取用户信息
              wx.getSetting({
                success: res => {
                  console.log('getSetting', res);
                  if (res.authSetting['scope.userInfo']) {
                    // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                    wx.getUserInfo({
                      lang: 'zh_CN',
                      success: (res) => {
                        console.log(res);
                        let UserInfo = res.encryptedData //拿的是加密的
                        let iv = res.iv
                        wx.request({
                          url: this.globalData.url + '/VeevlinkApplication/AppUserQuery.aspx',
                          method: 'POST',
                          data: {
                            appid: this.globalData.appid,
                            openId: OpenId,
                            code: Code,
                            userInfo: UserInfo,
                            iv: iv
                          },
                          header: {
                            'Content-Type': 'application-json'
                          },
                          success: (res) => {
                            console.log('获取信息', res.data);
                            wx.navigateTo({
                              url: '/pages/mainPage/login/main?id=1&type=' + orderPramas,
                              success: (res) => {
                                // 通过eventChannel向被打开页面传送数据
                                console.log('跳转页面成功', res);
                              },
                              fail: (res) => {
                                console.log('跳转页面失败', res);
                              }
                            })

                            //return;
                            // wxe3786e0f1c17f2f9
                            wx.setStorageSync('information' + this.globalData.appid, res.data);

                            wx.hideLoading();


                            let timestamp = Date.parse(new Date());
                            wx.setStorageSync('timestamp' + this.globalData.appid, timestamp);
                            // // 本地存储openId,时效为2小时
                            // wx.setStorageSync('openId', res.data)
                            success();
                          }
                        });

                      }
                    });
                  } else {
                    wx.authorize({
                      scope: 'scope.writePhotosAlbum',
                      success: (res) => { //这里是用户同意授权后的回调
                        console.log("授权成功" + res);
                        success(res)

                      },
                      fail: (res) => { //这里是用户拒绝授权后的回调
                        console.log(res);
                        error(res)
                      }
                    })
                  }
                }
              })
            }
          },
          fail: (err) => {
            error({
              text: '获取openid失败!',
              res: res.errMsg
            });
            console.log(err);
          }
        })
      } else {
        error({
          text: '登录失败!',
          res: res.errMsg
        })

        console.log('登录失败！' + res.errMsg)
      }

    },
    fail: (res) => {
      error({
        text: 'login登录失败!',
        res: res.errMsg
      })
      console.log('login登录失败', res)
    },
  })
}

export function VeevlinkClient(path, type, data, success, error, text, isShowLoading) {
  const veevlinkajax = () => {
    let list = wx.getStorageSync('information' + this.globalData.appid);
    
    let url
    if (list.EnableReverseProxy) {
      url = list.ReverseProxyUrl + '/services/apexrest' + path;
    } else {
      url = list.InstanceUrl + '/services/apexrest' + path;
    }
    console.log('url', url);
    let header = {
      'Authorization': 'Bearer ' +
        list.SessionId,
      // 'Bearer' : list.vue.SessionId,
      'Content-Type': 'application-json'
    };
    wx.showLoading({
      mask: true,
      title: text ? text : '加载中',
    });
    console.log('url', url);
    wx.request({
      url: url,
      method: type,
      data: data,
      header: header,
      success: (res) => {
        // 查看状态码 是 401 则重新获取session并运行逻辑
        console.log('状态码:' + res.statusCode);
        if (res.statusCode === 401) {
          function getSession() {
            wx.request({
              url: list.RefreshTokenProxyUrl + '?SourceId=' + list.AppId + '&openid=' + list.OpenId,
              method: 'GET',
              data: '',
              header: {
                'Content-Type': 'application-json'
              },
              success: (res2) => {
                list.SessionId = res2.data.access_token;
                console.log(res2.data.access_token);
                wx.setStorageSync('information' + this.globalData.appid, list);
                console.log(wx.getStorageSync('information' + this.globalData.appid));
                veevlinkajax()
              },
              fail: (err) => {
                getSession();
              }
            })
          }
          getSession();
          return;
        }

        let statusCode = JSON.stringify(res.statusCode);
        // 如果有error为true  就发邮件
        if (res.data.error) {
          let datas = {
            Url: path,
            Name: 'redstone',
            CreateAt: new Date().toLocaleString(),
            Body: JSON.stringify({
              Url: path,
              Name: 'redstone',
              context: JSON.stringify(list),
              body: JSON.stringify(data),
              CreateAt: new Date().toLocaleString(),
              errorMsg: JSON.stringify(res.data)
            })
          };
          wx.request({
            url: 'https://api.veevlink.com/Log/ErrorLog',
            method: 'POST',
            data: datas,
            header: {
              'Content-Type': 'application-json'
            },
            success: (res) => {

            },
            fail: (err) => {}
          });
          if (!isShowLoading) {
            wx.hideLoading();
          }
          error('网络有点问题,请稍后重试');
          return;
        }
        success(res.data);
        if (!isShowLoading) {
          wx.hideLoading()
        }

      },
      fail: (err) => {
        wx.getSystemInfo({
          success: (res) => {
            wx.getNetworkType({
              success: (res2) => {
                // 返回网络类型, 有效值：
                // wifi/2g/3g/4g/unknown(Android下不常见的网络类型)/none(无网络)
                let datas = {
                  Url: path,
                  Name: '8848',
                  CreateAt: new Date().toLocaleString(),
                  Body: JSON.stringify({
                    Url: path,
                    Name: '8848',
                    context: JSON.stringify(list),
                    CreateAt: new Date().toLocaleString(),
                    body: JSON.stringify(data),
                    errorMsg: JSON.stringify(err),
                    getSystemInfo: JSON.stringify(res),
                    getNetworkType: JSON.stringify(res2)
                  })
                };
                wx.request({
                  url: 'https://api.veevlink.com/Log/ErrorLog',
                  method: 'POST',
                  data: datas,
                  header: {
                    'Content-Type': 'application-json'
                  },
                  success: (res) => {

                  },
                  fail: (err) => {}
                });
              }
            });


          }
        });
        //
        if (this.globalData.appid === 'wxe3786e0f1c17f2f9') {
          error(err);
        } else {
          error('网络有点问题,请稍后重试');
        }
        if (!isShowLoading) {
          wx.hideLoading();
        }
      }
    })
  }
  veevlinkajax();
}

export function UploadClient(path, type, data, success, error) {
  let list = wx.getStorageSync('information' + this.globalData.appid);
  console.log(list);
  let url;
  if (list.EnableReverseProxy) {
    url = list.ReverseProxyUrl + '/services/apexrest' + path;
  } else {
    url = list.InstanceUrl + '/services/apexrest' + path;
  }

  let header = {
    'Authorization': 'Bearer ' + list.SessionId,
    // 'Bearer' : list.SessionId,
    'Content-Type': 'application-json'
  }
  wx.showLoading({
    title: '加载中',
  });
  wx.request({
    url: url,
    method: type,
    data: data,
    header: header,
    success: (res) => {
      success(res);
      wx.hideLoading();
    },
    fail: (err) => {
      error(err);
      wx.hideLoading();
    }
  })
}

// VeevlinkData
export function VeevlinkData() {
  return wx.getStorageSync('information' + this.globalData.appid);
}
export function formaldata() {
  return formal;
}
// OSS相关域名
export function VeevlinkOssUrl() {
  return this.globalData.url;
}
export function VeevlinkOssShowUrl() {
  return this.globalData.ossshowurl;
}
//oss测试系统显示图片
// export function testshowurl() {
//
// }

export function Appid() {
  return this.globalData.appid;
}
export function imgUrl() {
  return this.globalData.imgurl;
}

function VeevlinkClientnext(path, type, data, success, error) {
  let list = wx.getStorageSync('information' + this.globalData.appid);
  console.log(list);
  let url;
  if (list.EnableReverseProxy) {
    url = list.ReverseProxyUrl + '/services/apexrest' + path;
  } else {
    url = list.InstanceUrl + '/services/apexrest' + path;
  }
  let header = {
    'Authorization': 'Bearer ' + list.SessionId,
    // 'Bearer' : list.vue.SessionId,
    'Content-Type': 'application-json'
  }
  wx.showLoading({
    title: '加载中',
  });
  wx.request({
    url: url,
    method: type,
    data: data,
    header: header,
    success: (res) => {
      success(res.data);
      wx.hideLoading();
    },
    fail: (err) => {
      error(err);
      wx.hideLoading();
    }
  })
}

//生成随机码设备mac
export function getUUID(){
  let mac = wx.getStorageSync('uuid')
  if (mac) {
      return mac;
  } else {
      var s = [];
      var hexDigits = "0123456789abcdef";
      for (var i = 0; i < 36; i++) {
          s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1)
      }
      s[14] = "4";
      s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1)
      s[8] = s[13] = s[18] = s[23] = "-"
      var uuid = s.join("");
      wx.setStorageSync('uuid', uuid)
      // localStorage.setItem("uuid", uuid)
      return uuid;
  }
}
//生成32位随机数
export function randomWord() {
  var chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  var nums = "";
  for (var i = 0; i < 32; i++) {
  var id = parseInt(Math.random() * 61);
  nums += chars[id];
  }
  return nums;
  }
  export function Sort(obj) {//排序的函数
    var newkey = Object.keys(obj).sort();
　　//先用Object内置类的keys方法获取要排序对象的属性名，再利用Array原型上的sort方法对获取的属性名进行排序，newkey是一个数组
    var newObj = {};//创建一个新的对象，用于存放排好序的键值对
    for (var i = 0; i < newkey.length; i++) {//遍历newkey数组
        newObj[newkey[i]] = obj[newkey[i]];//向新创建的对象中按照排好的顺序依次增加键值对

    }
    return newObj;//返回排好序的新对象
}
//生成加密的参数x-sign
export function createSign(path,bodyJson,headerList){
  let that = this;
  var sb = '';
  let signUrl = '';
  let b = Sort(headerList);
   for(let i in b){
    if(b[i] && i !== "X-apiKey" && "bodySign" !== i && i !== "X-sign"){
      sb+=i +"="+b[i]+"&";
    }
   }
   var link = '';
   let c = Sort(bodyJson);
   for(let i in c){
    if(c[i] !== ''){
      link+=i +"="+c[i]+"&";
    }
   }
   var index = link.lastIndexOf('&');  
   var link1 = link.substring(0,index);
   console.log('接口名称------',path)
   console.log('前端传参------',bodyJson)
   var linkurl = zhmd5.md5(link1)
   signUrl = path + "?"+sb + "X-apiKey=e1e499e5746f90191ec5261947055e0c"+ '&bodySign='+linkurl.toUpperCase();
   console.log('signUrl------',signUrl) ;
  var sign =  zhmd5.md5(signUrl)
  let signlink = sign.toUpperCase();
  return signlink;
}
//接口请求封装
export function getRequest(path,data, success, error, text, isShowLoading){
  const veevlinkajax = () => {
  let that = this;
  let url = this.globalData.url + path;
  var timestamp = Date.parse(new Date());
  var random = this.randomWord();
  var uuid = this.getUUID();
  if(wx.getStorageSync('token')){
    // list["X-token"] = this.globalData.token
    this.globalData.token = wx.getStorageSync('token')
  }else{
     this.globalData.token = wx.getStorageSync('token')
  }
  var list = {
    'X-mac':uuid,//终端mac地址uuid
    'X-version':'1.0',//小程序设备版本号                                               
    'X-deviceType':'iphone',//设备类型
    'X-languageCode':'zh_CN',//语言代码
    'X-timestamp':timestamp,//时间戳
    'X-tenantCode':'100084602341',//租户code
    'X-apiId':'123456',//租户ID
    'X-apiKey':'123456',//参数加密秘钥
    'X-requestToken':random,//32位随机数
    'X-sign':'',//参数加密完后设置
    'X-token':this.globalData.token //token
  }
 
  console.log('token------',this.globalData.token);
  let signLink = this.createSign(path,data,list);
  if(path !== '/boapi/v1/course/keyword/list'){
    wx.showLoading({
      mask: true,
      title: text ? text : '加载中',
    });
  }
 
  //发起网络请求
  wx.request({
    url: url, //仅为示例，并非真实的接口地址
    data: data,
    method:'POST',
    header: {
      'content-type': 'application/json', // 默认值
      'X-mac':uuid,//终端mac地址uuid
      'X-version':'1.0',//小程序设备版本号                                               
      'X-deviceType':'iphone',//设备类型
      'X-languageCode':'zh_CN',//语言代码
      'X-timestamp':timestamp,//时间戳
      'X-tenantCode':'100084602341',//租户code
      'X-apiId':'123456',//租户ID
      'X-apiKey':'e1e499e5746f90191ec5261947055e0c',//参数加密秘钥
      'X-requestToken':random,//32位随机数
      'X-sign':signLink,//参数加密完后设置
      'X-token':this.globalData.token //token
    },
    success (res) {
      console.log('接口返回------',res.data)
      // let statusCode = JSON.stringify(res.statusCode);
      //判断接口返回状态码是否报错
        if (!isShowLoading) {
          wx.hideLoading();
        }
        if(res.data.resultCode === 0){
          success(res.data);
        }else{
          if(res.data.resultCode !== 2000){
                 wx.showModal({
            title: '提示',
            content: res.data.resultCode + ':'+ res.data.description,
            success (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
            
          }else{
            success(res.data);
          }
          return;
        }
        // error('网络有点问题,请稍后重试');
        
      },
      //接口访问失败
      fail: (err) => {
        wx.getSystemInfo({
          success: (res) => {
            wx.getNetworkType({
              success: (res2) => {
                // 返回网络类型, 有效值：
                // wifi/2g/3g/4g/unknown(Android下不常见的网络类型)/none(无网络)
                console.log('网络状态',res2)
                console.log('报错',err)
                wx.showModal({
                  title: '提示',
                  content: '网络有点问题，请稍后重试！',
                  success (res) {
                    if (res.confirm) {
                      console.log('用户点击确定')
                    } else if (res.cancel) {
                      console.log('用户点击取消')
                    }
                  }
                })
              
                
              }
            });


          }
        });

        if (!isShowLoading) {
          wx.hideLoading();
        }
      }
    })
  }
  veevlinkajax()
}

